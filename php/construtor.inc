<?php

//Localização HTML
$html = "static/html/";

//Localização PHP
$php = "php/";

//conexão banco
include "conecta.inc";

session_start();

if (empty($_GET["p"])) {
    $pagina = "inicio";
} else {
    $pagina = $_GET["p"];
}

if (!isset($_SESSION["nome"])) {
    $conteudo[] = $html . "login.html";

    if ($pagina == "processaLogin") {
        include $php . "paginas/processaLogin.inc";
    }
} else {
    $conteudo[] = $html . "cabecalho.html";
    switch ($pagina) {
        case "home":
            include $php . "paginas/processaHome.inc" ;
            break;
        case "atividade":
            $conteudo[] = $html . "atividade.html";
            break;
        case "usuario":
            $conteudo[] = $html . "usuario.html";
            break;
        case "processaAtividade":
            include $php . "paginas/processaAtividade.inc";
            break;
        case "processaUsuario":
            include $php . "paginas/processaUsuario.inc";
            break;
        case "alteraUsuario":
            include $php . "paginas/alteraUsuario.inc";
            break;
        case "alteraAtividade":
            include $php . "paginas/alteraAtividade.inc";
            break;
        case "edicao":
            $conteudo[] = $html . "edicao.html";
            break;
        case "processaEdicao":
            include $php . "paginas/processaEdicao.inc";
            break;
        case "index":
            session_destroy();
            $conteudo[] = $html . "login.html";
            break;           
        default :
            echo "Sinto muito";
            break;
    }
    $conteudo[] = $html . "rodape.html";
}

if($pagina == "index"){
    array_pop($conteudo);
    array_shift($conteudo);
}

foreach ($conteudo as $key) {
    include $key;
}