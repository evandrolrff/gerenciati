<?php
$sql = "SELECT nome, status FROM edicao;";
$retorno = mysql_query($sql, $conecta);
?>
<div class="container"> <!--container responsável pelo corpo do home-->
    </br></br></br></br></br>
    <div class="matshead">
        <h2 class="text-muted">Situação Edições</h2>
    </div>
    <hr class="featurette-divider">

    <?php
    while ($edicao = mysql_fetch_array($retorno)) {
        switch ($edicao["status"]) {
            case "Iniciada":               
                echo "<div class = 'row'><a href = '#'>
                <div class = 'alert alert-info alert-dismissible' role = 'alert'>
                <button type = 'button' class = 'close' data-dismiss = 'alert' aria-label = 'Close'>
                <span aria-hidden = 'true'>&times;</span></button><strong>Inicidada! 
                </strong>" . $edicao['nome'] . ".</div></a></div>";
                break;
            case "Atrasada":
                echo "<div class = 'row'><a href = '#'>
                <div class = 'alert alert-danger alert-dismissible' role = 'alert'>
                <button type = 'button' class = 'close' data-dismiss = 'alert' aria-label = 'Close'>
                <span aria-hidden = 'true'>&times;</span></button><strong>Atrasada! 
                </strong> " . $edicao['nome'] . ".</div></a></div>";
                break;
            case "Andamento":
                echo "<div class = 'row'><a href = '#'>
                <div class = 'alert alert-warning alert-dismissible' role = 'alert'>
                <button type = 'button' class = 'close' data-dismiss = 'alert' aria-label = 'Close'>
                <span aria-hidden = 'true'>&times;</span></button><strong>Andamento! 
                </strong> " . $edicao['nome'] . ".</div></a></div>";
                break;
            case "Concluida":
                echo "<div class='alert alert-success alert-dismissible' role='alert'>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span></button>
                <strong>Concluído! </strong>" . $edicao['nome'] . ".</div>";
                break;
            default:
                echo "<h3 class='text-muted'>Não há edições em andamento ou atrasadas.</h3>";
                break;
        }
    }
    mysql_free_result($retorno);
    ?>


    <div class="matshead">
        <h3 class="text-muted">Atividades Cadastradas</h3>
    </div>
    <?php
    $sql = "SELECT * FROM atividade;";
    $retorno = mysql_query($sql, $conecta);    
    ?>
    <hr class="featurette-divider">
    <div class="row">
        <div class="col-md-10">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Atividade</th>                            
                            <th>Descrição</th>
                            <th>Integrantes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php //AQUI DEVE SER MUDADO, PARA COLOCAR AS LINHAS E COLUNAS DENTRO DO LOOP EXECUTADO
                        $i = 1;                        
                        while ($atividade = mysql_fetch_array($retorno)) {
                            
                            echo "<tr onclick='linkAcao('./andmAtividade.html')'><td>". $i." </td><td>"
                                    .$atividade["nome"]."</td>".
                                    "<td>".$atividade["descricao"]."</td><td>";
                                    $sql2 = "SELECT nome FROM usuario natural join atvusuario "
                                            . "WHERE idatividade = " . $atividade['idatividade'] . ";";
                                    $retorno2 = mysql_query($sql2, $conecta);
                                    while ($integrantes = mysql_fetch_array($retorno2)) {
                                        echo $integrantes["nome"] . "<br>";
                                    }
                                    echo "</td></tr>";
                                    mysql_free_result($retorno2);
                                    $i++;
                        }
                        mysql_free_result($retorno);
                                    ?>                            
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> <!--fim do corpo do home-->
<script>
    /*funcao javascript redireciona ao clicar na linha da table
     *  para página especifica da atividade
     */
    function linkAcao(adress) {
        window.location.assign(adress);
    }
</script>